#! /bin/bash

INRES="1366x768"
OUTRES="640x480"
FPS="30"
QUAL="medium"
STREAM_KEY=$(cat ~/.twitch-key)

ffmpeg \
	-f x11grab -s $INRES -r "$FPS" -i :0.0 \
	-vcodec libx264 -s $OUTRES -preset $QUAL \
	-acodec libmp3lame -ar 44100 -threads 3 -qscale 3 -b 712000 -bufsize 512k \
	-f flv "rtmp://live.justin.tv/app/$STREAM_KEY"
